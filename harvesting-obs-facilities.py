# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Exploring Observation Facilities and Instruments in the IVOA registry and data discovery services.
#
# We explore the observation facility keywords that are listed in the IVOA Registry, as well as in ObsTAP and EPNTAP data discovery services.
#
# ---

# %%
from pyvo.dal import TAPService, DALServiceError, DALQueryError, DALFormatError
from collections import namedtuple
from requests.exceptions import ConnectionError
import json

# %% [markdown]
# We define two objects: 
# - `ObsSystem` to store pairs of keywords (facility and instrument)
# - 'Resource` to store the source of the record, its IVOID and table name (when applicable) 

# %%
ObsSystem = namedtuple('ObsSystem', ['facility', 'instrument'])
Resource = namedtuple('Resource', ['source_type', 'ivoid', 'table_name'])

# %%
facilities_ivo = dict()  # placeholder for IVOA registry extraction
facilities_obs = dict()  # placeholder for ObsTAP services extraction
facilities_epn = dict()  # placeholder for EPNTAP services extraction

# %% [markdown]
# The data will be stored in a `facilities` dictionary, with key equal to a tuple (containing paired facilities and instruments)

# %% [markdown]
# ### Loading `facility` and `instrument` lists from RegTAP

# %% [markdown]
# We use the relational registry managed by PADC: `http://voparis-rr.obspm.fr/tap`

# %%
rr_server = "http://voparis-rr.obspm.fr/tap"
rr_tap = TAPService(rr_server)
source_type = 'registry'

# %% [markdown]
# The following query is getting the (`facility`, `instrument`) pairs from the IVOA registry, for all registered resources. 

# %%
query = """select distinct t1.ivoid as ivoid, t1.detail_value as facility, t2.detail_value as instrument 
from rr.res_detail as t1 inner join rr.res_detail as t2 on t1.ivoid = t2.ivoid 
where t1.detail_xpath='/facility' and t2.detail_xpath='/instrument'"""

results = rr_tap.search(query)

for row in results:
    name = ObsSystem(row['facility'], row['instrument'])
    source = Resource(source_type, row['ivoid'], None)
    if name in facilities_ivo.keys():
        facilities_ivo[name].add(source)
    else:
        facilities_ivo[name] = {source}

# %%
print(f"Found: {len(facilities_ivo.keys())} distinct Facility/Instrument pairs in IVOA registry")
list(facilities_ivo.keys())

# %%
print("[EXAMPLE] List of resources claiming to provide HST/WFC3 related data:")
facilities_ivo[ObsSystem(facility='HST', instrument='WFC3')]

# %% [markdown]
# Dump into simple JSON file for further processing

# %%
with open('facilities_ivoareg.json', 'w') as f:
    json.dump([s._asdict() for s in facilities_ivo.keys()], f)


# %% [markdown]
# ### Loading `facility` and `instrument` lists from ObsTAP services 

# %% [markdown]
# First get the list of ObsTAP services in the registry

# %%
query = """SELECT table_name, access_url, ivoid FROM ( SELECT ivoid, table_name FROM rr.res_table WHERE
 table_name = 'ivoa.obscore') as ivotables
NATURAL JOIN rr.capability
NATURAL JOIN rr.interface WHERE
standard_id = 'ivo://ivoa.net/std/tap' AND intf_role='std'"""

obscore_results = rr_tap.search(query)


# %%
print(obscore_results)

# %% [markdown]
# Since some services may not respond and some queries are may take some time to reply, we collect the status of each service and store it, so that we don't run the same query again, in case one of them fails.  

# %%
status = dict([(row['ivoid'], None) for row in obscore_results])

# %%
source_type = 'obscore'

for table in obscore_results:
    
    url = table['access_url']
    ivoid = table['ivoid']
    
    print(url)
    if status[ivoid] is None:
        try: 
            tap_service = TAPService(url)

            query = "SELECT distinct facility_name as facility, instrument_name as instrument FROM ivoa.obscore"
            results = tap_service.run_async(query)

            for row in results: 
                name = ObsSystem(row['facility'], row['instrument'])
                source = Resource(source_type, table['ivoid'], table['table_name'])
                if name in facilities_obs.keys():
                    facilities_obs[name].add(source)
                else:
                    facilities_obs[name] = {source}

            status[ivoid] = "ok"

        except DALServiceError as e:
            print(e)
            status[ivoid] = str(e)
        except DALQueryError as e:
            print(e)
            status[ivoid] = str(e)
        except DALFormatError as e:
            print(e)
            status[ivoid] = str(e)
        except ConnectionError as e:
            print(e)
            status[ivoid] = str(e)

    else:
        print("skipping, already included")

# %%
status

# %%
print(f"Found: {len(facilities_obs.keys())} distinct Facility/Instrument pairs in obscore tables")
list(facilities_obs.keys())

# %% [markdown]
# Dump into simple JSON file for further processing

# %%
with open('facilities_obscore.json', 'w') as f:
    json.dump([s._asdict() for s in facilities_obs.keys()], f)


# %% [markdown]
# ### Loading `instrument_host_name` and `instrument_name` lists from EPN-TAP services

# %% [markdown]
# **NB1**: the correct `table_utype` is `ivo://ivoa.net/std/epntap#table-2.0`, but many resources have an old `table_utype`, predating the IVOA recommendation adoption; some have erroneous strings; and some have no `table_utype` (and thus can‘t be found automatically).
#
# **NB2**: since there can be several `epn_core` tables in a TAP server, we group the tables by `access_url` (TAP endpoint)

# %%
query = """SELECT table_name, access_url, ivoid FROM ( SELECT ivoid, table_name, table_utype FROM rr.res_table WHERE
 table_utype like 'ivo://vopdc.obspm/std/epncore%' OR table_utype LIKE 'ivo://ivoa.net/std/epntap#table-2.%' OR table_utype='ivo.//vopdc.obspm/std/epncore#schema-2.0') as epntables
NATURAL JOIN rr.capability
NATURAL JOIN rr.interface WHERE
standard_id = 'ivo://ivoa.net/std/tap' AND intf_role='std'"""
epncore_results = rr_tap.search(query)

source_type = 'epncore'


epncore_tables = {}
for row in epncore_results:
    access_url = row['access_url']
    if access_url in epncore_tables.keys(): 
        epncore_tables[access_url].append((row['ivoid'], row['table_name']))
    else:
        epncore_tables[access_url] = [(row['ivoid'], row['table_name'])]


# %%
print("list of all EPNTAP services discovered in the IVOA registry, sorted by their table name:")
sorted(epncore_results["table_name"])

# %%
# adding tables (incorreclty declared in the registry): 

# major services:
epncore_tables['https://archives.esac.esa.int/psa-tap/tap'] = [('ivo://esavo/psa/epntap', 'epn_core')]
epncore_tables['http://tapvizier.cds.unistra.fr/TAPVizieR/tap'] = [('ivo://cds.vizier/b/planets', '\"B/planets/epn_core\"')]

epncore_tables

# %%
for url, tables in epncore_tables.items():
    print(url)
    try: 
        tap_service = TAPService(url)

        for ivoid, table in tables:
            print(table)
            if table.startswith("sshade"): 
                print("skip...")
                continue
            
            query = f"select distinct instrument_host_name, instrument_name from {table}" 
            results = tap_service.search(query)
                
            for row in results: 
                name = ObsSystem(row['instrument_host_name'], row['instrument_name'])
                source = Resource(source_type, ivoid, table)
                if name in facilities_epn.keys():
                    facilities_epn[name].add(source)
                else:
                    facilities_epn[name] = {source}
                        
    except DALServiceError as e:
        print(e)
    except DALQueryError as e:
        print(e)
    except DALFormatError as e:
        print(e)

# %%
print(f"Found: {len(facilities_epn.keys())} distinct Facility/Instrument pairs in epncore tables")
list(facilities_epn.keys())

# %%
print("List tables providing data products with `instrument_host_name`='Hubble':") 
[(key, facilities_epn[key]) for key in facilities_epn.keys() if key.facility == 'Hubble']

# %%
print("List tables providing data products with `instrument_host_name`='HST':") 
[(key, facilities_epn[key]) for key in facilities_epn.keys() if key.facility == 'HST']

# %%
print("List tables providing data products with `instrument_host_name`='Ulysses':") 
[(key, facilities_epn[key]) for key in facilities_epn.keys() if key.facility == 'Ulysses']

# %% [markdown]
# Dump into simple JSON file for further processing

# %%
with open('facilities_epncore.json', 'w') as f:
    json.dump([s._asdict() for s in facilities_epn.keys()], f)

# %%
