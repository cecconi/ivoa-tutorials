# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
from pyvo.dal import TAPService
from pyvo.dal.adhoc import DatalinkResults

from collections import namedtuple
EpnTapServiceInfo = namedtuple("EpnTapServiceInfo", ["tap_access_url", "table_name"])

# %%
from matplotlib import pyplot as plt

# %%
from urllib.request import urlopen
import io
from astropy.io.votable import parse

# %%
spicam_epntap = EpnTapServiceInfo("http://vo.projet.latmos.ipsl.fr/tap", "spicam.epn_core")
#mcd_epntap = EpnTapServiceInfo("http://vo.lmd.jussieu.fr/tap", "mcd.epn_core")

# %% [markdown]
# __TODO__: next tutorial with ipyaladin to selected profile from mars map 

# %% [markdown]
# ### Loading SPICAM metadata

# %% [markdown]
# First, let's connect to the TAP server hosting the SPICAM EPNcore table, and retrieve the number of records. 

# %%
service = TAPService(spicam_epntap.tap_access_url)
query = f"select count(*) as len from {spicam_epntap.table_name}"
result = service.search(query)
print(f"SPICAM service: {result[0]['len']} records")

# %% [markdown]
# Query the SPICAM EPNcore table to get the metadata of temperature profile corresponing to observation #2888A1.

# %%
query = f"""select granule_uid, granule_gid, obs_id, measurement_type, access_url, datalink_url
from {spicam_epntap.table_name} where obs_id = '2888A1' and measurement_type = 'phys.temperature'"""
result = service.search(query)
epncore_result = result.to_table()

# %%
epncore_result

# %%
selected_result = epncore_result[0]

# %%
spicam_data = parse(io.BytesIO(urlopen(selected_result['access_url']).read())).get_first_table().to_table()

# %%
plt.plot(spicam_data['Temp_T100k'], spicam_data['AAMZD'])
plt.ylabel('altitude (km)')
plt.xlabel('temperature (K)')
plt.title('SPICAM Temperature profile')

# %% [markdown]
# ### Using Datalink to retrieved modeled data
#
# We select the scenario 28 (Mars Year 28) from the MCD model output 

# %%
dl = DatalinkResults.from_result_url(selected_result['datalink_url'])
dl.to_table()
for link in dl:
    print(link['description'])


# %%
for link in dl:
    if link['description'].startswith('MY28'):
        selected_mcd_datalink = link

# %%
selected_mcd_datalink['description']

# %%
mcd_data = parse(io.BytesIO(urlopen(selected_mcd_datalink['access_url']).read())).get_first_table().to_table()

# %%
plt.plot(mcd_data['Temperature'], mcd_data['AAMZD'], '--', label="MCD")
plt.plot(spicam_data['Temp_T100k'], spicam_data['AAMZD'], label="SPICAM")
plt.ylabel('altitude (km)')
plt.legend()
plt.xlabel('temperature (K)')
plt.ylim((40,150))
plt.title('SPICAM Temperature profile')

# %%
